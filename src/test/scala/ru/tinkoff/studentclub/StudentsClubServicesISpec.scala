package ru.tinkoff.studentclub

import org.scalatest.concurrent.Futures
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}
import ru.tinkoff.studentclub.dao.db.Schema

import scala.None
import scala.collection.Seq
import scala.concurrent.Await
import scala.concurrent.duration.Duration

abstract class StudentsClubServicesISpec extends AsyncFlatSpec with StudentsClubServices with Matchers with Futures {
  "client registration service" should "register client" in {
    val clientPhone = "+1000"
    val clientName = "lol"
    clientRegistrationService.register(clientName, clientPhone)
      .map { client =>
        client.name shouldBe clientName
        client.phone shouldBe clientPhone
      }
  }

  //TODO executor registration test
  "executor registration service" should "register executor" in {
    val executorName = "Executor"
    val executorPhone = "+0-000-000-00-00"
    executorRegistrationService.register(executorName, executorPhone)
      .map{ executor =>
        executor.name shouldBe executorName
        executor.phone shouldBe executorPhone
      }
  }

  "order service" should "return statistics by client" in { // TODO fix it
    for {
      (lol, kek) <- clientRegistrationService.register("lol", "+999") zip
        executorRegistrationService.register("kek", "+777")
      order <- orderService.create("Борщ", 100, lol.id)
      _ <- orderService.assign(order.id, kek.id)
      stats <- orderService.sumByClients().map(map => map.filter(_._1 == lol.id))
    } yield {
      stats shouldBe Map(lol.id -> BigDecimal(100))
    }
  }

  // TODO one click orders test
  "oneClick order service" should "create order and client" in {
    for {
      order <- oneClickOrderService.createOneClick("client", "+111", "order", 1000)
      _ <- orderDao.find(order.id).map(_ shouldBe Some(order))
      resultClient <- clientDao.exists(order.clientId).map(_ shouldBe true)
    } yield {
      resultClient
    }
  }
}

class ImMemoryStudentsClubServicesISpec extends StudentsClubServicesISpec with InMemoryStudentsClub

class DbStudentsClubServicesISpec extends StudentsClubServicesISpec with SlickStudentsClub with BeforeAndAfterAll with Futures {
  override protected def beforeAll(): Unit = {
    Await.result(Schema.setup(), Duration.Inf)
  }
}
