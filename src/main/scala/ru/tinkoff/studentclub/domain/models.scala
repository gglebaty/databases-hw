package ru.tinkoff.studentclub.domain

import java.util.UUID

case class Client(id: UUID, name: String, phone: String)

case class Executor(id: UUID, name: String, phone: String)

case class Order(id: UUID, description: String, price: BigDecimal, clientId: UUID, executorId: Option[UUID])