package ru.tinkoff.studentclub.registration.client

import java.util.UUID

import ru.tinkoff.studentclub.dao.ClientDao
import ru.tinkoff.studentclub.domain.Client

import scala.concurrent.{Future, ExecutionContext}

class ClientRegistrationService(clientDao: ClientDao)
                               (implicit ec: ExecutionContext) {

  def register(name: String, phone: String): Future[Client] = {
    val client = Client(UUID.randomUUID(), name, phone)

    clientDao
      .insert(client)
      .map(_ => client)
  }
}
