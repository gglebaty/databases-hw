package ru.tinkoff.studentclub.registration.executor

import java.util.UUID

import ru.tinkoff.studentclub.dao.ExecutorDao
import ru.tinkoff.studentclub.domain.Executor

import scala.concurrent.{Future, ExecutionContext}

class ExecutorRegistrationService(executorDao: ExecutorDao)
                                 (implicit ec: ExecutionContext) {

  def register(name: String, phone: String): Future[Executor] = {
    val executor = Executor(UUID.randomUUID(), name, phone)
    executorDao
      .insert(executor)
      .map(_ => executor)
  }
}
