package ru.tinkoff.studentclub

import ru.tinkoff.studentclub.dao.db.{SlickOneClickOrderDao, SlickExecutorDao, SlickClientDao, SlickOrderDao}
import ru.tinkoff.studentclub.dao.{OneClickOrderDao, ExecutorDao, ClientDao, OrderDao}
import ru.tinkoff.studentclub.dao.inmemory.{InMemoryClientDao, InMemoryOrderDao, InMemoryOneClickOrderDao, InMemoryExecutorDao}
import ru.tinkoff.studentclub.order.{OneClickOrderService, OrderService}
import ru.tinkoff.studentclub.registration.client.ClientRegistrationService
import ru.tinkoff.studentclub.registration.executor.ExecutorRegistrationService

import scala.concurrent.ExecutionContext.Implicits.global

trait StudentsClubServices {

  lazy val orderService: OrderService = {
    new OrderService(clientDao, orderDao)
  }

  lazy val clientRegistrationService: ClientRegistrationService = {
    new ClientRegistrationService(clientDao)
  }

  lazy val executorRegistrationService: ExecutorRegistrationService = {
    new ExecutorRegistrationService(executorDao)
  }

  lazy val oneClickOrderService: OneClickOrderService = {
    new OneClickOrderService(oneClickOrderDao)
  }

  protected def orderDao: OrderDao
  protected def executorDao: ExecutorDao
  protected def clientDao: ClientDao
  protected def oneClickOrderDao: OneClickOrderDao
}

trait InMemoryStudentsClub extends StudentsClubServices {
  override protected lazy val clientDao = new InMemoryClientDao
  override protected lazy val executorDao = new InMemoryExecutorDao
  override protected lazy val orderDao = new InMemoryOrderDao
  override protected lazy val oneClickOrderDao = new InMemoryOneClickOrderDao(orderDao, clientDao)
}

trait SlickStudentsClub extends StudentsClubServices {
  override protected lazy val orderDao: SlickOrderDao = new SlickOrderDao
  override protected lazy val executorDao: SlickExecutorDao = new SlickExecutorDao
  override protected lazy val clientDao: SlickClientDao = new SlickClientDao()
  override protected lazy val oneClickOrderDao: SlickOneClickOrderDao = new SlickOneClickOrderDao()
}