package ru.tinkoff.studentclub.dao

import java.util.UUID

import ru.tinkoff.studentclub.domain.Order

import scala.concurrent.Future

trait OrderDao {
  def sumByClients(): Future[Map[UUID, BigDecimal]]

  def insert(order: Order): Future[Unit]

  def find(id: UUID): Future[Option[Order]]

  def list(clientId: UUID): Future[Seq[Order]]

  def assign(orderId: UUID, executorId: UUID): Future[Unit]
}
