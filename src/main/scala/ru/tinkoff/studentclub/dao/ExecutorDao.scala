package ru.tinkoff.studentclub.dao

import ru.tinkoff.studentclub.domain.Executor

import scala.concurrent.Future

trait ExecutorDao {
  def insert(executor: Executor): Future[Unit]
}
