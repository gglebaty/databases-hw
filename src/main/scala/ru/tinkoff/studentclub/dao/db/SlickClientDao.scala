package ru.tinkoff.studentclub.dao.db

import java.util.UUID

import ru.tinkoff.studentclub.dao.ClientDao
import ru.tinkoff.studentclub.domain.Client

import scala.concurrent.{Future, ExecutionContext}


class SlickClientDao(implicit ec: ExecutionContext) extends ClientDao {

  import Schema.clients
  import Schema.db
  import Schema.profile.api._ // syntax for slick dsl

  override def insert(client: Client): Future[Unit] =
    db.run(clients += client).map(_ => ())

  override def exists(clientId: UUID): Future[Boolean] = {
    db.run(clients.filter(_.id === clientId).exists.result)
  }


}
