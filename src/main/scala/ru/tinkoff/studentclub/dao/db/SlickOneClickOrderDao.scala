package ru.tinkoff.studentclub.dao.db

import ru.tinkoff.studentclub.dao.OneClickOrderDao
import ru.tinkoff.studentclub.domain.{Client, Order}

import scala.concurrent.{ExecutionContext, Future}

class SlickOneClickOrderDao(implicit ec: ExecutionContext) extends OneClickOrderDao {
  import Schema.clients
  import Schema.orders
  import Schema.db
  import slick.jdbc.H2Profile.api._

  // Use dbAction.andThan(nextDbAction).transactionally
  override def insertAll(client: Client, order: Order): Future[Unit] = //TODO
    db.run((clients += client).andThen(orders += order).transactionally).map(_ => ())

}
