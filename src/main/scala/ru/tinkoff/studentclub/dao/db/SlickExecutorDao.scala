package ru.tinkoff.studentclub.dao.db

import ru.tinkoff.studentclub.dao.ExecutorDao
import ru.tinkoff.studentclub.domain.Executor

import scala.concurrent.{ExecutionContext, Future}

class SlickExecutorDao(implicit ec: ExecutionContext) extends ExecutorDao {
  import Schema.executors
  import Schema.db
  import slick.jdbc.H2Profile.api._

  override def insert(executor: Executor): Future[Unit] =  // TODO
    db.run(executors += executor).map(_ => ())
}
